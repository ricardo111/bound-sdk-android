package id.bound.sdk


import android.content.Context
import android.os.Build
import android.util.Log
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import id.bound.sdk.network.Client
import org.json.JSONObject
import java.io.IOException

/**
 * BoundSDK main entry point.
 *
 * Usage example
 * ```
 * BoundSDK.initializeSdk(requireContext())
 * private val boundSdk = BoundSDK.getInstance()
 *
 * boundSdk.openCheckUrl(checkUrl)
 * ```
 */
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class BoundSDK private constructor(context: Context) {
    private val client = Client(context)

    /**
     * Execute a phone check verification, by performing a network request against the Mobile carrier
     * over mobile data connection.
     *
     * Invokes the request immediately, and blocks until the response can be processed or is in error.
     *
     * Prerequisites:
     * Get the mobile application user's phone number and create a PhoneCheck via the bound.ID API
     * in order to receive a unique `check_url` in the response.
     * Request the `check_url` on the mobile device over the mobile data connection.
     *
     * @param checkUrl The phone check url.
     *
     * @WorkerThread
     * @throws IOException if the request could not be executed due to cancellation, a connectivity
     *     problem or timeout. Because networks can fail during an exchange, it is possible that the
     *     remote server accepted the request before the failure.
     * @throws IllegalStateException when the call has already been executed.
     */
    @Throws(java.io.IOException::class)
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun openCheckUrl(@NonNull checkUrl: String) {
        if (BuildConfig.DEBUG) {
            Log.i(TAG, "Triggering check url")
        }
        client.requestSync(url = checkUrl, method = "GET")
    }

    /**
     * Execute a network call to a specified [endpoint].
     * Invokes the GET request immediately, and blocks until the response can be processed or is in error.
     *
     * Example usage: Read the IP address of the device over the mobile connection, in order to determine
     * if bound.ID has reachability for the network.
     *
     * @WorkerThread
     * @return The response as a JSONObject, or null if response cannot be processed.
     */
    fun getJsonResponse(@NonNull endpoint: String): JSONObject? {
        Log.d(TAG, "getJsonResponse for endpoint:$endpoint")
        return client.requestSync(url = endpoint, method = "GET")
    }

    /**
     * Execute a network call to a specified [endpoint].
     * Invokes the GET request immediately, and blocks until the response can be processed or is in error.
     *
     * Example usage: Read the IP address of the device over the mobile connection, in order to determine
     * if Bound has reachability for the network.
     *
     * @WorkerThread
     * @return The value mapped by [key] if it exists, coercing it if necessary, or the empty string
     * if no such mapping exists.
     */
    fun getJsonPropertyValue(@NonNull endpoint: String, @NonNull key: String) : String? {
        Log.d(TAG, "getJsonPropertyValue for endpoint:$endpoint key:$key")
        val jsonResponse = getJsonResponse(endpoint)
        return jsonResponse?.optString(key)
    }

    companion object {
        private const val TAG = "BoundSDK"
        private var instance: BoundSDK? = null

        @Synchronized
        fun initializeSdk(context: Context): BoundSDK {
            var currentInstance = instance
            if (null == currentInstance) {
                currentInstance = BoundSDK(context)
            }
            instance = currentInstance
            return currentInstance
        }

        @Synchronized
        fun getInstance(): BoundSDK {
            val currentInstance = instance
            checkNotNull(currentInstance) {
                BoundSDK::class.java.simpleName +
                        " is not initialized, call initializeSdk(...) first"
            }
            return currentInstance
        }
    }
}
